public class Tantsupaar {
  private String poisiNimi;
  private String tydrukuNimi;
  private int poisiSynniAasta;
  private int tydrukuSynniAasta;

  public String getPoisiNimi() {
    return poisiNimi;
  }

  public void setPoisiNimi(String poisiNimi) {
    this.poisiNimi = poisiNimi;
  }

  public String getTydrukuNimi() {
    return tydrukuNimi;
  }

  public void setTydrukuNimi(String tydrukuNimi) {
    this.tydrukuNimi = tydrukuNimi;
  }

  public int getPoisiSynniAasta() {
    return poisiSynniAasta;
  }

  public void setPoisiSynniAasta(int poisiSynniAasta) {
    this.poisiSynniAasta = poisiSynniAasta;
  }

  public int getTydrukuSynniAasta() {
    return tydrukuSynniAasta;
  }

  public void setTydrukuSynniAasta(int tydrukuSynniAasta) {
    this.tydrukuSynniAasta = tydrukuSynniAasta;
  }

  public Tantsupaar(String poisiNimi, String tydrukuNimi, int poisiSynniAasta, int tydrukuSynniAasta) {
    this.poisiNimi = poisiNimi;
    this.tydrukuNimi = tydrukuNimi;
    this.poisiSynniAasta = poisiSynniAasta;
    this.tydrukuSynniAasta = tydrukuSynniAasta;
  }

  public int vanemaPartneriVanus(int aastaArv) {
    int vanemPartner = Math.min(poisiSynniAasta, tydrukuSynniAasta);

    return aastaArv - vanemPartner;
  }

  public boolean syndinudSamalAastal() {
    return poisiSynniAasta == tydrukuSynniAasta;
  }

  public String perenimed() {
    String[] poisinimeTykid = poisiNimi.split(" ");
    String[] tydrukunimeTykid = tydrukuNimi.split( " ");

    return poisinimeTykid[poisinimeTykid.length - 1] + " "
        + tydrukunimeTykid[tydrukunimeTykid.length - 1];
  }

  @Override public String toString() {
    return "Tantsuparnterid: " + perenimed();
  }

}
