public class Main {

    public static void main(String[] args) {
        Tantsupaar t1 = new Tantsupaar("Meelis Rand", "Elis Kallas",
            1992, 1995);

        Tantsupaar t2 = new Tantsupaar("Kalle Maasikas", "Eeva Lote",
            1996, 1996);

        System.out.println(t1.syndinudSamalAastal());
        System.out.println(t1.vanemaPartneriVanus(2018));
        System.out.println(t1.toString());


        System.out.println(t2.syndinudSamalAastal());
        System.out.println(t2.vanemaPartneriVanus(2018));
        System.out.println(t2.toString());


        IDKaart id1 = new IDKaart("39211010222", "Leelo", "A15829");

        System.out.println(id1.synnikuupaev());
    }
}
