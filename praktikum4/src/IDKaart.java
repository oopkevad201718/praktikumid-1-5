public class IDKaart {

  private String isikukood;
  private String omanikuNimi;
  private String kaardiNumber;

  public String getIsikukood() {
    return isikukood;
  }

  public void setIsikukood(String isikukood) {
    this.isikukood = isikukood;
  }

  public String getOmanikuNimi() {
    return omanikuNimi;
  }

  public void setOmanikuNimi(String omanikuNimi) {
    this.omanikuNimi = omanikuNimi;
  }

  public String getKaardiNumber() {
    return kaardiNumber;
  }

  public void setKaardiNumber(String kaardiNumber) {
    this.kaardiNumber = kaardiNumber;
  }

  public IDKaart(String isikukood, String omanikuNimi, String kaardiNumber) {
    this.isikukood = isikukood;
    this.omanikuNimi = omanikuNimi;
    this.kaardiNumber = kaardiNumber;
  }

  public String isikuSugu() {
    char esimeneNumber = this.isikukood.charAt(0);
    if (esimeneNumber == '1' || esimeneNumber == '3' || esimeneNumber == '5') {
      return "mees";
    } else if(esimeneNumber == '2' || esimeneNumber == '4' || esimeneNumber == '6') {
      return "naine";
    }
    return "Anomaalia";
  }

  public String synnikuupaev() {
    String aasta = this.isikukood.substring(1, 3);
    String kuu = this.isikukood.substring(3, 5);
    String paev = this.isikukood.substring(5, 7);

    char esimeneNumber = this.isikukood.charAt(0);

    if (esimeneNumber == '1' || esimeneNumber == '2') {
      aasta = "18" + aasta;
    } else if (esimeneNumber == '3' || esimeneNumber == '4'){
      aasta = "19" + aasta;
    } else { // TODO: Peaks lisama ka 21, 22 jms
      aasta = "20" + aasta;
    }


    return paev + "." + kuu + "." + aasta;
  }
}
