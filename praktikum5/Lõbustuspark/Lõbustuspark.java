import java.util.List;

public class Lõbustuspark {
  List<Lõbustus> lõbustused;

  public Lõbustuspark(List<Lõbustus> lõbustus) {
    this.lõbustused = lõbustus;
  }

  public void alustaSeiklust(Külastaja külastaja) {
    System.out.println("Alustan seiklust!");

    for(Lõbustus lõbustus: lõbustused) {
      lõbustus.lõbusta(külastaja);
    }

    for(String kirjeldus : külastaja.kõikKirjeldused()) {
      System.out.println(kirjeldus);
    }

    System.out.println("Kulud: " + külastaja.kuludeSumma() + "€");
  }
}
