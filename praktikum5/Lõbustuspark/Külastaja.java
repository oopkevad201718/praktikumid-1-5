import java.util.ArrayList;
import java.util.List;

public class Külastaja {
  private List<String> külastuseKirjeldused;
  private int vanus;
  private double kulud;

  public int getVanus() {
    return vanus;
  }

  public Külastaja(int vanus) {
    this.külastuseKirjeldused = new ArrayList<>();
    this.vanus = vanus;
  }

  public void lisaKirjeldus(String kirjeldus) {
    this.külastuseKirjeldused.add(kirjeldus);
  }

  public void lisaKulu(double kulu) {
    this.kulud += kulu;
  }

  public List<String> kõikKirjeldused() {
    return this.külastuseKirjeldused;
  }

  public double kuludeSumma() {
    return this.kulud;
  }
}
