public class TasulineLõbustus implements Lõbustus {
  private double hind;
  private Lõbustus lõbustus;

  public TasulineLõbustus(double hind, Lõbustus lõbustus) {
    this.hind = hind;
    this.lõbustus = lõbustus;
  }

  @Override public void lõbusta(Külastaja külastaja) {
    lõbustus.lõbusta(külastaja);
    külastaja.lisaKulu(this.hind);
    külastaja.lisaKirjeldus("Tasusin külastuse eest " + this.hind + "€");
  }
}
