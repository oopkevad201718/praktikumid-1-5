public class VanuseKontrollija implements Lõbustus {
  private int nõutudVanus;
  private Lõbustus lõbustus;

  public VanuseKontrollija(int nõutudVanus, Lõbustus lõbustus) {
    this.nõutudVanus = nõutudVanus;
    this.lõbustus = lõbustus;
  }

  @Override public void lõbusta(Külastaja külastaja) {
    if (this.nõutudVanus > külastaja.getVanus()) {
      külastaja.lisaKirjeldus("Ei läbinud vanusekontrolli!");
    }
    else {
      this.lõbustus.lõbusta(külastaja);
    }
  }
}
