import java.util.ArrayList;
import java.util.List;

public class Test {
  public static void main(String[] args) {
    Vaateratas vaateratas = new Vaateratas();
    Lasketiir lasketiir = new Lasketiir();
    Kloun kloun = new Kloun("Piip");
    LõbustaKloun lõbustaKloun = new LõbustaKloun(kloun);
    TasulineLõbustus tasulineVaateratas = new TasulineLõbustus(2.25, vaateratas);
    TasulineLõbustus tasulineLasketiir = new TasulineLõbustus(1.5, lasketiir);
    VanuseKontrollija vanuseKontrollija = new VanuseKontrollija(10, tasulineLasketiir);

    List<Lõbustus> lõbustused = new ArrayList<>();
    lõbustused.add(tasulineVaateratas);
    lõbustused.add(vanuseKontrollija);
    lõbustused.add(lõbustaKloun);

    Lõbustuspark lõbustuspark = new Lõbustuspark(lõbustused);
    Külastaja külastaja1 = new Külastaja(9);
    Külastaja külastaja2 = new Külastaja(11);

    lõbustuspark.alustaSeiklust(külastaja1);
    System.out.println("----------------------");
    lõbustuspark.alustaSeiklust(külastaja2);

  }
}
