public class LõbustaKloun implements Lõbustus {
  private Kloun kloun;

  public LõbustaKloun(Kloun kloun) {
    this.kloun = kloun;
  }

  @Override public void lõbusta(Külastaja külastaja) {
    kloun.esine(külastaja);
  }
}
