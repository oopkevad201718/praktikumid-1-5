import java.util.Comparator;

public class Puuvili implements Comparable<Puuvili>{
  private String nimetus;
  private int kogus;

  public Puuvili(String nimetus, int kogus) {
    this.nimetus = nimetus;
    this.kogus = kogus;
  }

  public String getNimetus() {
    return nimetus;
  }

  public int getKogus() {
    return kogus;
  }

  @Override public int compareTo(Puuvili o) {
    int võrdleKogust = o.getKogus();
    return this.kogus - o.getKogus(); //Kasvav
    //return o.getKogus() - this.kogus; //Kahanev
  }

  @Override public String toString() {
    return "Nimetus: " + this.getNimetus() + ", kogus: " + this.getKogus() + "kg.";
  }

  public static Comparator<Puuvili> PuuviljadKahanevas = new Comparator<Puuvili>() {
    public int compare(Puuvili p1, Puuvili p2) {
      return p2.getKogus() - p1.getKogus() ;
    }
  };

  public static Comparator<Puuvili> PuuviljadKasvavas = new Comparator<Puuvili>() {
    public int compare(Puuvili p1, Puuvili p2) {
      return p1.getKogus() - p2.getKogus() ;
    }
  };
}
