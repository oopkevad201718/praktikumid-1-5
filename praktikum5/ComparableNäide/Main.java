import java.util.Arrays;

public class Main {


        public static void main(String[] args) {
            Puuvili[] puuviljad = new Puuvili[3];
            puuviljad[0] = new Puuvili("Õun", 10);
            puuviljad[1] = new Puuvili("Pirn", 5);
            puuviljad[2] = new Puuvili("Mango", 25);

            System.out.println("Enne sorteerimist:");
            for(Puuvili p : puuviljad) {
                System.out.println(p.toString());
            }
            System.out.println("--------------------------");

            System.out.println("Default sorteerimine:");
            Arrays.sort(puuviljad);
            for(Puuvili p : puuviljad) {
                System.out.println(p.toString());
            }

            System.out.println("--------------------------");

            System.out.println("Puuviljad kahanevas järjekorras:");
            Arrays.sort(puuviljad, Puuvili.PuuviljadKahanevas);
            for(Puuvili p : puuviljad) {
                System.out.println(p.toString());
            }

            System.out.println("--------------------------");

            System.out.println("Puuviljad kasvavas järjekorras:");
            Arrays.sort(puuviljad, Puuvili.PuuviljadKasvavas);
            for(Puuvili p : puuviljad) {
                System.out.println(p.toString());
            }
        }

}
