import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

public class FailiTest {
  public static void main(String[] args) throws IOException {
    //Käsurealt pathi andmine: Run -> Edit Configurations -> Program arguments

    Path kaust = Paths.get(args[0]);
    if (!Files.isDirectory(kaust)) {
      System.out.println("Tegu ei ole kaustaga");
      return;
    }

    Failivaatleja failivaatleja = new Failivaatleja();
    Files.walkFileTree(kaust, failivaatleja);

    Collections.sort(failivaatleja.getFailiNimed());
    System.out.println("Tähestikulises järjekorras:");

    for(String name: failivaatleja.getFailiNimed()) {
      System.out.println(name);
    }

    System.out.println("---------------------");
    System.out.println("Tähestikulises järjekorras tagurpidi");
    Collections.sort(failivaatleja.getFailiNimed(), new NimeVõrdleja());

    for(String name: failivaatleja.getFailiNimed()) {
      System.out.println(name);
    }
  }
}
