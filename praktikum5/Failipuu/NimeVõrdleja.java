import java.util.Comparator;

public class NimeVõrdleja implements Comparator<String> {
  @Override public int compare(String a, String b) {
    int võrdlus = a.compareTo(b);

    if (võrdlus < 0) {  // a on väiksem
      return 1; // Tagasta 1 ehk ütleme, et a peaks olema peale b, kuna tahame saada vastupidist järjekorda.
    }
    else if (võrdlus > 0){ // a on suurem
      return -1;
    }
    return 0;
  }
}
