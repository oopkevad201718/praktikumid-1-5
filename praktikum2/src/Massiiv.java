import java.util.Arrays;

public class Massiiv {

  public static void main(String[] args) {
    int[] m1 = {-10, 17, 21};
    int[] m2 = {-2, 5, 17, 28};

    System.out.println(Arrays.toString(merge(m1, m2)));
    System.out.println(Arrays.toString(reverse(m1)));
  }

  public static int[] reverse(int[] m){
    int[] result = new int[m.length];
    for (int i = 0; i < m.length; i++) {
      result[i] = m[m.length - 1 - i];
    }

    return result;
  }

  public static int[] merge(int[] m1, int[] m2) {
    int[] result = new int[m1.length + m2.length];
    int loendur1 = 0; //Loendur mitu elementi me esimesest massiivist oleme võtnud.
    int loendur2 = 0;

    for (int i = 0; i < result.length; i++) {
      if (loendur1 >= m1.length) { // Võta teisest massiivist, esimesest on otsas
        result[i] = m2[loendur2];
        loendur2++;
        continue;
      }

      if (loendur2 >= m2.length) { // Võta esimesest, teises on otsas.
        result[i] = m1[loendur1];
        loendur1++;
        continue;
      }

      if (m1[loendur1] < m2[loendur2]) {
        result[i] = m1[loendur1];
        loendur1++;
      } else {
        result[i] = m2[loendur2];
        loendur2++;
      }
    }

    return result;
  }
}
