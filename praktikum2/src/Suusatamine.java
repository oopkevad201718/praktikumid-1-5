public class Suusatamine {

  public static void main(String[] args) {
    int[] stardiNumbrid = {2, 1, 4, 5, 8, 7, 3, 6};
    double[] stopperiNaidud = {148.2, 150.4, 165.7, 181.2, 226.0, 231.9, 0.0, 0.0};

    int[] stardiAjad = arvutaStardiAjad(stardiNumbrid);
    double[] läbimisAjad = arvutaLäbimisAjad(stardiAjad, stopperiNaidud);
    int võitjaIndex = leiaVõitjaIndex(läbimisAjad);
    int katkestajaid = leiaKatkestajateArv(stopperiNaidud);

    System.out.println("Võitja stardinumber oli " + stardiNumbrid[võitjaIndex] + " ja tema aeg oli " + läbimisAjad[võitjaIndex]);
    System.out.println("Kakestajaid oli " + katkestajaid);
  }

  public static int leiaKatkestajateArv(double[] stopperiNäidud) {
    int counter = 0;
    for (double näit: stopperiNäidud) {
      if (näit == 0.0) {
        counter++;
      }
    }
    return  counter;
  }

  public static int leiaVõitjaIndex(double[] ajad) {
    double väikseim = ajad[0];
    int index = 0;

    for (int i = 1; i < ajad.length; i++) {
      if (ajad[i] == 0.0) {
        continue;
      }

      if (ajad[i] < väikseim) {
        väikseim = ajad[i];
        index = i;
      }
    }
    return index;
  }

  public static double[] arvutaLäbimisAjad(int[] stardiAjad, double[] stopperiNaidud) {
    double[] result = new double[stardiAjad.length];
    for (int i = 0; i < stardiAjad.length; i++) {
      if (stopperiNaidud[i] == 0.0) {
        result[i] = 0.0;
        continue;
      }
      result[i] = Math.round((stopperiNaidud[i] - stardiAjad[i]) * 100) / 100.0;
    }

    return result;
  }

  public static int[] arvutaStardiAjad(int[] stardiNumbrid) {
    int[] result = new int[stardiNumbrid.length];

    for(int i = 0; i < stardiNumbrid.length; i++) {
      result[i] = (stardiNumbrid[i] - 1) * 15;
    }
    return result;
  }
}
