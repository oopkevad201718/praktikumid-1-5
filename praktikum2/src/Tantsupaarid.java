import static java.util.Arrays.sort;

public class Tantsupaarid {

  public static void main(String[] args) {
    int[] poisid = {180, 175, 200, 172, 169, 183, 188};
    int[] tüdrukud = {165, 167, 172, 169, 162};

    tekitaPaarid(poisid, tüdrukud);
  }

  public static void tekitaPaarid(int[] poisid, int[] tüdrukud) {
    // Sorteerimine pikkuste järgi kasvavas jrks.
    sort(poisid);
    sort(tüdrukud);

    // Loome sõne massiivi lühima massiivi pikkuse järgi.
    String[] paarid = new String[Math.min(poisid.length, tüdrukud.length)];

    // Tekitame paarid ja lisame massiivi
    for (int i = 0; i < paarid.length; i++) {
      paarid[i] = "(" + poisid[i] + "," + tüdrukud[i] + ")";
    }

    System.out.println("Saime paarid:");
    //Liidame massiivi elemendid omavahel kokku, ühendajaks on ", ".
    System.out.println(String.join(", ", paarid));

    // Väljastame need, kes üle jäid (kui jäid)
    if (poisid.length != tüdrukud.length) {
      // Tekitame massiivi, mille pikkuseks on paariliseta jäänud isikute arv.
      String[] üle = new String[Math.abs(poisid.length - tüdrukud.length)];
      int counter = 0; //abi muutuja massiivi lisamiseks

      if (poisid.length > tüdrukud.length) {
        System.out.println("Üle jäid poisid");
        // i algväärtus on paaride arv, kuna peame vaatama neid ainult, kes üle jäid
        for (int i = paarid.length; i < poisid.length; i++) {
          üle[counter] = String.valueOf(poisid[i]);
          counter++;
        }
      } else {
        System.out.println("Üle jäid tüdrukud");
        for (int i = paarid.length; i < tüdrukud.length; i++) {
          üle[counter] = String.valueOf(tüdrukud[i]);
          counter++;
        }
      }

      System.out.println(String.join(", ", üle));
    }
  }
}
